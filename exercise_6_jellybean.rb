require_relative "exercise_5_dessert"

class JellyBean<Dessert
  attr_accessor :flavor

  def initialize(name,calories,flavor)
    super(name,calories)
    @flavor = flavor
  end 

  def delicious?
    @flavor.to_s != "black licorice"
  end

  def to_s
    "#{@name} #{@calories} #{@flavor}"
  end
end