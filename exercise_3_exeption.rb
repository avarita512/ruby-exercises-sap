class WrongNumberOfPlayersError < StandardError ; end
class NoSuchStrategyError < StandardError ; end

def rps_game_winner(game)
  raise WrongNumberOfPlayersError if game.length > 2
  strategy = ['R', 'P', 'S']
  raise NoSuchStrategyError unless strategy.include?(game[0][1]) && strategy.include?(game[1][1])
  winer = case
    when game[0][1] == 'R' && game[1][1] == 'S' then game[0]
    when game[0][1] == 'P' && game[1][1] == 'R' then game[0]
    when game[0][1] == 'S' && game[1][1] == 'P' then game[0]
    when game[0][1] == game[1][1] then game[0]
    else game[1] 
  end
end