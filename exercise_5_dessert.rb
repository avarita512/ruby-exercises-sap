class  Dessert
  attr_accessor :name, :calories

  def initialize(name,calories)
    @name, @calories = name,calories
  end

  def healthy?
    @calories.to_i < 200
  end

  def delicious?
	true
  end

  def to_s
    "#{@name} #{@calories}"
  end
end
