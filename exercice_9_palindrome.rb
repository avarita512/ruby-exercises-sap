class String
  def palindrome?
	processed_string = self.downcase.gsub(/\W/, '')
  	processed_string == processed_string.reverse
  end
end

module Enumerable 
  def palindrome?
	a = self.to_a
	a == a.reverse
  end
end

