def count_words(string)
  words = Hash.new(0)
  string.downcase.scan(/\w+/).each { |word| words[word] += 1 }
  words
end