class Numeric
  @@rate = {'dollar' => 32.26, 'euro' => 43.61, 'ruble' => 1}
  def method_missing(name, *args, &block)
    if name == :in
      transfer_in = args[0].to_s.gsub( /s$/, '')
      if @@rate.has_key?(transfer_in)
        self / @@rate[transfer_in]
      end
    else
      single_currency = name.to_s.gsub( /s$/, '')
      if @@rate.has_key?(single_currency)
        self * @@rate[single_currency]
      end
    end
  end
end