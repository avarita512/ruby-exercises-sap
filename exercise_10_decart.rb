class CartesianProduct
  include Enumerable

  def initialize(sequence1, sequence2)
    @result = []
    unless sequence1 == [] || sequence2 == []
      sequence1.collect {|q| sequence2.collect {|w| @result << [q,w] } }
    end
  end

  def each(&block)
    @result.each(&block)
  end
end